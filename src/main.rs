use std::{io::Write, process::{Command, Stdio}};

fn s(n: u64) -> usize {
    for i in 1..61 {
        if (2 as u64).pow(i as u32) % ((n - 1) as u64) == 1 {
            return i;
        }
    }
    0
}

fn main() {
    let mut gp = Command::new("gp")
        .arg("-q")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();
    {
        let stdin = gp.stdin.as_mut().unwrap();
        stdin
            .write_all(b"fordiv(2^60 - 1, n, print(n))\nquit\n")
            .unwrap();
    }
    let divisors: Vec<u64> = String::from_utf8(gp.wait_with_output().unwrap().stdout)
        .unwrap()
        .lines()
        .map(|x| x.parse::<u64>().unwrap())
        .collect();
    let mut sum = 0;
    for m in divisors {
        let x = m + 1;
        if s(x) == 60 {
            sum += x;
        }
    }
    println!("{}", sum);
}
